﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Repository;
using System;
using System.Windows.Forms;

namespace DrugsInteraction
{
    public partial class InsertInteraction : Form
    {
        public InsertInteraction()
        {
            InitializeComponent();
        }

        private void btnAddInteraction_Click(object sender, EventArgs e)
        {
            if (checkBoxes())
            {
                MedicamentRepository.Instance.Interactions.Add(new SubSubInteraction(txtSub1.Text.ToLower(), txtSub2.Text.ToLower(), txtInteraction.Text));
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private bool checkBoxes()
        {
            if (txtSub1.Text != "" && txtSub2.Text != "" && txtInteraction.Text != "")
            {
                if (txtSub1.Text != txtSub2.Text)
                    return true;
                else
                    MessageBox.Show("Insert different active supstances.", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                MessageBox.Show("All textboxes must be filled.", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            return false;
        }
    }
}
