﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Domain.Enums;
using DrugsInteraction.Repository;
using System;
using System.Windows.Forms;

namespace DrugsInteraction
{
    public partial class InsertTypeTypeInteraction : Form
    {
        public InsertTypeTypeInteraction()
        {
            InitializeComponent();
        }

        private void InsertTypeTypeInteraction_Load(object sender, EventArgs e)
        {
            cmbxType1.DataSource = Enum.GetValues(typeof(MedicamentTypes));
            cmbxType1.SelectedItem = null;
            cmbxType2.DataSource = Enum.GetValues(typeof(MedicamentTypes));
            cmbxType2.SelectedItem = null;
        }

        private void btnAddInteraction_Click(object sender, EventArgs e)
        {
            if (checkBoxes())
            {
                MedicamentTypes type1, type2;
                Enum.TryParse<MedicamentTypes>(cmbxType1.SelectedValue.ToString(), out type1);
                Enum.TryParse<MedicamentTypes>(cmbxType2.SelectedValue.ToString(), out type2);
                MedicamentRepository.Instance.TypeTypeInteractions.Add(new TypeTypeInteraction(type1, type2, txtInteraction.Text));
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        private bool checkBoxes()
        {
            if (cmbxType1.SelectedItem != null && cmbxType2.SelectedItem != null && txtInteraction.Text != "")
            {
                if ((MedicamentTypes)cmbxType1.SelectedItem == (MedicamentTypes)cmbxType2.SelectedItem)
                {
                    MessageBox.Show("Select different types.", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                    return true;
            }
            else
                MessageBox.Show("All textboxes must be filled.", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            return false;
        }
    }
}
