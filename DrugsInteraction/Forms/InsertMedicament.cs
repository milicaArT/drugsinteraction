﻿using DrugsInteraction.Domain.Entities;
using DrugsInteraction.Domain.Enums;
using DrugsInteraction.Repository;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DrugsInteraction
{
    public partial class InsertMedicament : Form
    {
        private List<string> substances;
        public InsertMedicament()
        {
            InitializeComponent();
        }

        private void InsertMedicament_Load(object sender, EventArgs e)
        {
            substances = new List<string>();

            cmbxType.DataSource = Enum.GetValues(typeof(MedicamentTypes));
            cmbxType.SelectedItem = null;
        }

        private void btnAddSub_Click(object sender, EventArgs e)
        {
            if (txtActiveSubstance.Text != "")
            {
                if (!substances.Contains(txtActiveSubstance.Text.ToLower()))
                {
                    substances.Add(txtActiveSubstance.Text.ToLower());
                    lbxActive.Items.Add(txtActiveSubstance.Text.ToLower());
                    txtActiveSubstance.Text = "";
                }
                else
                {
                    MessageBox.Show("You have already inserted this active supstance!", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnAddMed_Click(object sender, EventArgs e)
        {
            if (txtName.Text != "")
            {
                MedicamentTypes type;
                Enum.TryParse<MedicamentTypes>(cmbxType.SelectedValue.ToString(), out type);
                MedicamentRepository.Instance.Medicaments.Add(new Medicament(txtName.Text, type, substances));
                DialogResult = DialogResult.OK;
                Close();
            }
            else
                MessageBox.Show("Insert medicament name and active substances!", "Warrning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
