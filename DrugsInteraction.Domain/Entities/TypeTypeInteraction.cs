﻿using DrugsInteraction.Domain.Enums;

namespace DrugsInteraction.Domain.Entities
{
    public class TypeTypeInteraction
    {
        public MedicamentTypes MedType1 { get; set; }
        public MedicamentTypes MedType2 { get; set; }
        public string Description { get; set; }

        public TypeTypeInteraction(MedicamentTypes mt1, MedicamentTypes mt2, string des)
        {
            MedType1 = mt1;
            MedType2 = mt2;
            Description = des;
        }
    }
}
