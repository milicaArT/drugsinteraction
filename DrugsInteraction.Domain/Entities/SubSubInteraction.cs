﻿namespace DrugsInteraction.Domain.Entities
{
    public class SubSubInteraction
    {
        public string ActiveSubstance1;
        public string ActiveSubstance2;
        public string Description;

        public SubSubInteraction(string as1, string as2, string des)
        {
            ActiveSubstance1 = as1;
            ActiveSubstance2 = as2;
            Description = des;
        }
    }
}
